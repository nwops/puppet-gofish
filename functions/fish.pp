# returns the top card from the deck or undef if no cards
function go_fish::fish($deck) {
  if $deck.count > 0 {
    return $deck[0]
  }
  return undef
}
