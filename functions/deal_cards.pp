# returns a double array of cards for each player
# @return Hash[]
# @example
# {"player1"=>["6_U", "8_U", "1_H", "3_H", "10_S", "2_H", "3_S"],
#  "player2"=>["J_U", "10_C", "K_U", "10_H", "1_S", "2_S", "2_U"],
# "player3"=>["9_C", "7_H", "6_H", "1_U", "10_U", "1_C", "Q_U"],
# "player4"=>["7_U", "4_H", "J_C", "8_S", "7_S", "8_C", "8_H"]}
function gofish::deal_cards(Array[String] $cards, Integer $hand_size, Array[String] $players) {
  $num_cards = $hand_size * $players.count
  $sample_cards = $cards.slice($num_cards)[0]
  $draw_pile = $cards.slice($num_cards)[1]
  $dealt = $sample_cards.slice($hand_size)
  $hands = $players.reduce({}) | Hash $acc, String $name | {
    $item_num = $acc.count
    $acc.merge({ $name => $dealt[$item_num] })
  }
  return [$hands, $draw_pile]
}
