function gofish::write($file, $data) {
  file{$file:
    ensure  => present,
    content => $data,
  }
  return $file
}
