function gofish::create_players(Integer $num_players) {
  $players = range(1, $num_players)
  $players.map | Integer $num | { "player_${num}" }
}
