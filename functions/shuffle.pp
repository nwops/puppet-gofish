function gofish::shuffle($face_cards, $num_cards, $suites) {
  $cards = $face_cards + $num_cards
  $result = $suites.map | String $suite | {
    $cards.map | String $card | {
      "${card}_${suite}"
    }
  }.flatten.shuffle.shuffle.reverse
  return $result
}
