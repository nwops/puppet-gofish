define gofish::game(
  Array[String] $player_names,
  Array[String] $deck, # this is the initial shuffled deck, which will never change
) {
  
  $result = gofish::deal_cards($deck, $player_names)
  $players_hands = $result[0]
  $draw_pile = $result[1]

  # would prefer to break out of a while loop
  range(1,100).each | Integer $round_num | {
    debug::break()
    gofish::round{"round_${round_num}":
      players   => $players_hands,
      draw_pile => $draw_pile,
    }
  }
}
