class gofish(
  String $game_name = 'sample',
  Integer $num_players = 2,
  Array[String] $player_names = gofish::create_players($num_players)
) {
  include gofish::deck
  gofish::game{$game_name:
    player_names => $player_names,
    deck         => $gofish::deck::cards
  }
}

# official rules
# https://bicyclecards.com/how-to-play/go-fish/

