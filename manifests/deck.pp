class gofish::deck(
  $face_cards = ['A', 'J', 'K', 'Q'],
  $num_cards = [
    '2', '3', '4',
    '5', '6', '7', '8',
    '9', '10'
  ],
  $suites = ['C', 'S', 'D', 'H']
) {
  $cards = shuffle($face_cards, $num_cards, $suites)
}
