require "spec_helper"

describe "gofish::shuffle" do
  face_cards = ["A", "J", "K", "Q"]
  num_cards = [
    "2", "3", "4",
    "5", "6", "7", "8",
    "9", "10",
  ]
  suites = ["C", "S", "D", "H"]

  let(:result) do
    ["K_D", "4_D", "4_S", "2_D", "10_C", "10_S", "9_D",
     "5_H", "J_S", "7_H", "A_H", "5_D", "Q_S", "J_H",
     "K_C", "9_S", "4_C", "8_C", "9_C", "3_H", "8_H",
     "2_C", "K_H", "5_S", "7_D", "J_C", "6_H", "8_S",
     "9_H", "3_D", "4_H", "Q_C", "2_H", "A_S", "J_D",
     "10_H", "A_C", "3_C", "Q_H", "7_C", "Q_D", "6_S",
     "6_C", "K_S", "5_C", "10_D", "2_S", "7_S", "8_D",
     "6_D", "A_D", "3_S"]
  end

  let(:args) do
    [face_cards, num_cards, suites]
  end

  it do
    is_expected.to run.with_params(*args).and_return(result)
  end
end
