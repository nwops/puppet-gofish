require 'spec_helper'

describe 'go_fish::fish' do
  let(:deck) do
    'some_value_goes_here'
  end
  it { is_expected.to run.with_params(deck).and_return('some_value') }
end
