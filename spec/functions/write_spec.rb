require 'spec_helper'

describe 'gofish::write' do
  let(:file) do
    'some_value_goes_here'
  end
  it { is_expected.to run.with_params(file).and_return('some_value') }
end
