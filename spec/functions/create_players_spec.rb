require 'spec_helper'

describe 'gofish::create_players' do
  let(:num_players) do
    'some_value_goes_here'
  end
  it { is_expected.to run.with_params(num_players).and_return('some_value') }
end
